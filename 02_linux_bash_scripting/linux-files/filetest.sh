#!/bin/bash
files="$@"

if [ -n "$files" ]; then
  for file in $files; do
    if [ ! -f "$file" ]; then
      echo "File does not exist: $file"
    elif [ ! -s "$file" ]; then
      echo "Permanently Deleting: $file, hope you didn't need it."
      rm -f $file
    else
	    echo "Inspected by: $USER DATE/TIME: $(date +%Y-%m-%d/%H:%M:%S)" >> $file
    fi
  done
else
  echo "Error no arguments passed to script"
fi
  
