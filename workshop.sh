#!/bin/bash
txtnum=0
imgnum=0

inception() {
dir=$1
for file in ${dir}/*; do
  if [ ! -d "${file}" ] && [[ "${file: -4}" == ".png" || "${file: -4}" == ".jpg" || "${file: -4}" == ".bmp" || "${file: -4}" == ".gif" || "${file: -5}" == ".jpeg" ]]; then
    echo "${file} is a image file and will be counted"
    ((imgnum++))
  elif [ ! -d "${file}" ] && [ "${file: -4}" == ".txt" ]; then
    echo "${file} is a .txt file and will be counted"
    ((txtnum++))
  elif [ -d "${file}" ]; then
    inception ${file}
  else
    echo "Not a directory or text file ignoring..."
  fi
done	      
}



inception "$1"

echo "Number of image files: ${imgnum}"
echo "Number of txt files: ${txtnum}"
