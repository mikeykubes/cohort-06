#!/bin/bash

txtnum=$(find /home/mikehogan/gitsource/cohort-06_bak/ -name *.txt | wc -l)
imgnum=$(find /home/mikehogan/gitsource/cohort-06_bak/ -regex ".*\.\(jpe?g\|png\|gif\|bmp\)" -type f | wc -l) 

echo "Number of image files: ${imgnum}"
echo "Number of txt files: ${txtnum}"
