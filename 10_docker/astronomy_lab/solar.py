#!/usr/bin/env python3
import requests
import argparse
from datetime import datetime
from pprint import pprint
from requests.models import HTTPBasicAuth


ASTRONOMYAPI_ID="c24f965e-d754-4e7f-ad38-06c793c2d279" 
ASTRONOMYAPI_SECRET="b586c68a0970564b265d639a9c730e1f1e8c7ac5e098e76d79c3e9f6807ce68804bf55e9f765943365a28b2766decc9b25cb7783f8ff3e984108e74528e04b91a6a88c8ec91055661d17d04c40de5213a98f773d3b8c586d5a78b10b9a517861d60985dcbcb20f767b614fab98d15301"

def get_location():
    """Returns the longitude and latitude for the location of this machine.
    
    Returns:
    str: latitude
    str: longitude
    str: state
    str: city"""

    response=requests.get("http://ip-api.com/json/",verify=False)
    response.raise_for_status()
    latitude = response.json()["lat"]
    longitude = response.json()["lon"]
    state = response.json()["region"]
    city = response.json()["city"]

    return latitude, longitude, state, city


def get_position(latitude, longitude):
    """Returns the current position of the sun in the sky at the specified location
    Parameters:
    latitude (str)
    longitude (str)
    
    Returns:
    float: azimuth
    float: altitude
    float: distance
    float: magnitude
    str: azimuth_string
    str: altitude_string
    """

    query = {
	"latitude": latitude,
	"longitude": longitude,
	"elevation": 0,
	"from_date": current_date,
	"to_date": current_date,
	"time": time
	}

    astro = requests.get(f'https://api.astronomyapi.com/api/v2/bodies/positions/{celestial}', auth=HTTPBasicAuth(ASTRONOMYAPI_ID, ASTRONOMYAPI_SECRET), params=query)
    astro.raise_for_status()
    astro_data = astro.json()['data']['table']['rows'][0]['cells'][0]
    azimuth = astro_data['position']['horizonal']['azimuth']['degrees']
    azimuth_string = astro_data['position']['horizonal']['azimuth']['string']
    altitude = astro_data['position']['horizonal']['altitude']['degrees']
    altitude_string = astro_data['position']['horizonal']['altitude']['string']
    distance = astro_data['distance']['fromEarth']['km']
    magnitude = astro_data['extraInfo']['magnitude']


    return float(azimuth), float(altitude), round(float(distance)), float(magnitude), azimuth_string, altitude_string


def print_position(azimuth, altitude, state, city, distance, magnitude, azimuth_string, altitude_string):
    """Prints the position of the sun in the sky using the supplied coordinates
    
    Parameters:
    azimuth (float)
    altitude (float)
    state (str)
    city (str)
    distance (float)
    magnitude (float)
    azimuth_string (str)
    altitude_string (str)"""

    now = datetime.now()
    current_date_lst = current_date.split('-')
    time_lst = time.split(':')
    obs_dt = datetime(int(current_date_lst[0]), int(current_date_lst[1]), int(current_date_lst[2]), int(time_lst[0]), int(time_lst[1]))
    print(f"From {city}, {state} at", (now.strftime("%I:%M %p")), "on", now.strftime("%B %d, %Y"))
    print('Your IP address is located at latitude:', ip_latitude, 'and longitude', ip_longitude, end='\n\n')
    print(f"Observer latitude: {latitude}, longitude: {longitude} at", obs_dt.strftime("%I:%M %p"), "on", obs_dt.strftime("%B %d, %Y"))
    print(f"{celestial} was at:", azimuth, "degrees azimuth,", altitude, "degrees altitude on the date observed.", end='\n\n')
    print(f'{celestial}:')
    print('\tDistance from Earth:', format(distance, ','), "km")
    print('\tMagnitude:', magnitude)
    print('Position:')
    print('\tAzimuth:', azimuth_string)
    print('\tAltitude:', altitude_string)

if __name__ == "__main__":
    try: 
        now = datetime.now()
        current_date = now.strftime("%G-%m-%d")
        time = now.strftime("%X")
        celestial = 'Sun'
        latitude, longitude, state, city = get_location()
        ip_latitude, ip_longitude = latitude, longitude
        parser = argparse.ArgumentParser(add_help=True)
        parser.add_argument('--at')
        parser.add_argument('--from_pos')
        parser.add_argument('celestial', nargs='?')
        args = parser.parse_args()
        if args.at:
            time_date = args.at.split('T')
            time = time_date[1]
            current_date = time_date[0]
        if args.from_pos:
            position = args.from_pos.split(',')
            latitude = position[0].split('=')[1]
            longitude = position[1].split('=')[1]
        if args.celestial:
            celestial = args.celestial.capitalize()
            
        azimuth, altitude, distance, magnitude, azimuth_string, altitude_string = get_position(latitude, longitude)
        print_position(azimuth, altitude, state, city, distance, magnitude, azimuth_string, altitude_string)

    except IndexError:
        print('There was an index error')
    except NameError:
        print('A name is not defined, please check your code')
    except AttributeError:
        print('There is an issue with an attribute, please check your code')
    except KeyError:
        print('Please check your command line arguments for errors and try again')
    except requests.exceptions.HTTPError as errh:
        print ("Http Error:",errh)
    except requests.exceptions.ConnectionError as errc:
        print ("Error Connecting:",errc)
    except requests.exceptions.Timeout as errt:
        print ("Timeout Error:",errt)
    except requests.exceptions.RequestException as err:
        print ("OOps: Something Else",err)
